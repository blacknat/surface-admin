/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : surface_admin

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 29/12/2021 12:47:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for z_admin
-- ----------------------------
DROP TABLE IF EXISTS `z_admin`;
CREATE TABLE `z_admin`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户密码',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '注册时间',
  `status` enum('0','1') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '账号状态 0封号 1正常',
  `update_time` int(11) NOT NULL DEFAULT 0,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '头像',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_admin
-- ----------------------------
INSERT INTO `z_admin` VALUES (1, 'admin', 'admin', '$2y$10$UybQERZCiMdchq1JEOi6tObvKqa69L.O9IA9m6xf0J4iJC2jwUo56', 1562810827, '1', 1605865528, '');
INSERT INTO `z_admin` VALUES (2, 'lalala', '啦啦啦', '$2y$10$fPMVUnyeCC1TTbU81DtpqO3f0kxwhuyYLUCwtz2ADt7OpMwmvr8wC', 1535080343, '1', 1640748903, '/uploads/20210619/bc7698478f5c416de6e26a96d497e09fa54ea08a.jpg');
INSERT INTO `z_admin` VALUES (3, 'zhangsan', '张二狗', '$2y$10$ZXhtX7QaB6LYDtU/ASkry.3Qu1.eaORNC8MtYHzIbBQl6UPRkQQXW', 1535080343, '1', 1640748909, '');

-- ----------------------------
-- Table structure for z_attachment
-- ----------------------------
DROP TABLE IF EXISTS `z_attachment`;
CREATE TABLE `z_attachment`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uuid` char(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'filehash',
  `uploader` int(11) NOT NULL DEFAULT 0 COMMENT '上传者',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '物理路径',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小',
  `mime` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'mime类型',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建日期',
  `ip` bigint(11) NOT NULL DEFAULT 0 COMMENT '上传者IP',
  `width` int(11) NOT NULL COMMENT '宽度',
  `height` int(11) NOT NULL COMMENT '高度',
  `suffix` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片类型',
  `domain` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '域名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_attachment
-- ----------------------------
INSERT INTO `z_attachment` VALUES (5, 'bc7698478f5c416de6e26a96d497e09fa54ea08a', 0, '/uploads/20210619/bc7698478f5c416de6e26a96d497e09fa54ea08a.jpg', 'avatar.jpg', 1208, 'image/jpeg', 1624113532, 2130706433, 300, 300, 'jpg', '');
INSERT INTO `z_attachment` VALUES (9, '081d4be3f47ac535e53edd56a430818339235cbe', 1, '/uploads/20210901/081d4be3f47ac535e53edd56a430818339235cbe.png', '1314887-20180110115220847-2035000717.png', 11636, 'image/png', 1630502434, 2130706433, 150, 150, 'png', '');
INSERT INTO `z_attachment` VALUES (10, 'a60a75bedada514c6a21a108b23dafc350a57ff0', 1, '/uploads/20211229/a60a75bedada514c6a21a108b23dafc350a57ff0.jpg', 'src=http___b-ssl.duitang.com_uploads_item_201809_14_20180914175026_KnnEP.thumb.700_0.jpeg&refer=http___b-ssl.duitang.jpg', 22784, 'image/jpeg', 1640745383, 2130706433, 700, 700, 'jpg', '');
INSERT INTO `z_attachment` VALUES (11, 'f181c761e8da16b1fa982e9d779f1f493a298171', 1, '/uploads/20211229/f181c761e8da16b1fa982e9d779f1f493a298171.jpg', 'src=http___img.jj20.com_up_allimg_tx28_41011424179667.jpg&refer=http___img.jj20.jpg', 26242, 'image/jpeg', 1640745566, 2130706433, 400, 400, 'jpg', '');
INSERT INTO `z_attachment` VALUES (12, '55cc0c68356b50880737326c1916ece5e3d9c2c6', 1, '/uploads/20211229/55cc0c68356b50880737326c1916ece5e3d9c2c6.jpg', 'src=http___img.jj20.com_up_allimg_tx28_41011424129648.jpg&refer=http___img.jj20.jpg', 19913, 'image/jpeg', 1640745630, 2130706433, 400, 400, 'jpg', '');
INSERT INTO `z_attachment` VALUES (13, 'dc6eb27c92a378a7f03cd2e9c02f8845a56dc9b2', 1, '/uploads/20211229/dc6eb27c92a378a7f03cd2e9c02f8845a56dc9b2.jpg', 'src=http___up.enterdesk.com_edpic_e8_ea_6b_e8ea6b847337f93065011c9b7c84ac03.jpg&refer=http___up.enterdesk.jpg', 38863, 'image/jpeg', 1640746101, 2130706433, 1024, 1024, 'jpg', '');
INSERT INTO `z_attachment` VALUES (14, '48ad6ea0ab6ee6739673a0fe77928b333632ea73', 1, '/uploads/20211229/48ad6ea0ab6ee6739673a0fe77928b333632ea73.jpg', 'src=http___c-ssl.duitang.com_uploads_item_201509_09_20150909170928_BFrmL.jpeg&refer=http___c-ssl.duitang.jpg', 315108, 'image/jpeg', 1640746413, 2130706433, 1920, 1280, 'jpg', '');

-- ----------------------------
-- Table structure for z_post
-- ----------------------------
DROP TABLE IF EXISTS `z_post`;
CREATE TABLE `z_post`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '描述',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `sort` smallint(6) NOT NULL COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  `create_time` int(11) NOT NULL COMMENT '发布时间',
  `update_time` int(11) NOT NULL COMMENT '修改时间',
  `user_id` int(11) NOT NULL COMMENT '发布者',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_post
-- ----------------------------
INSERT INTO `z_post` VALUES (7, '城市风光视频', '城市风光视频', '<p><iframe src=\"http://curd.demo.zsw.ink/static/backend/bg.mp4\"></iframe></p>', 0, 0, 1584282332, 1625368083, 1);
INSERT INTO `z_post` VALUES (8, '啦啦啦', '啦啦啦啦啦啦', '<p style=\"text-align:center;\">&nbsp;哈哈哈<br/></p>', 0, 0, 1584282332, 1625367976, 2);
INSERT INTO `z_post` VALUES (9, '可爱短发妹子俏皮', '可爱短发妹子俏皮', '<p><img src=\"/uploads/20211229/48ad6ea0ab6ee6739673a0fe77928b333632ea73.jpg\" style=\"max-width:50%;\"/><br/></p>', 0, 0, 1584282332, 1625367863, 2);
INSERT INTO `z_post` VALUES (18, 'VR，风再起时', 'VR，风再起时', '<p><span style=\"font-size: 1em;\">图片来源@视觉中国</span><br/></p><p><br/></p><p><br/></p><blockquote><p>文丨锦缎</p></blockquote><p><br/></p><p><br/></p><p>芒格说，一生抓住少数几个投资机会就足够了。诸如我们之前讨论的新能源、物联网，都是能够持续数年乃至数十年的超级投资机会。抓住这些机遇，将是实现财富增值和阶层跃迁难得的捷径。</p><p><br/></p><p><br/></p><p>关于VR，不少人却是谈虎色变。</p><p><br/></p><p><br/></p><p>依稀记得15年暴风集团连拉29个涨停板，一时间VR概念名声大噪，席卷A股市场；16年，又被誉为VR元年，资本蜂拥而入，互联网巨子、手机寡头Facebook、谷歌、微软、腾讯、三星、Sony、苹果、华为等等纷纷开始布局，一片繁华景象，泡沫堆积……</p><p><br/></p><p><br/></p><p>不到一年光景，产品的不成熟和部分厂商恶意割韭菜行为让VR市场急转直下，泡沫破裂一片死寂，纵使扎克伯格喊出“希望10亿人使用Oculus头盔”，消费者依然选择用脚投票。</p><p><br/></p><p><br/></p><p>人们心中的成见是一座大山，在山中待久了慧眼会蒙上尘埃。所以，近期元宇宙概念的兴起和华为5G+AR峰会的召开，投资者观点两极分化严重，不少人以为又是虚伪的黄粱一梦。</p><p><br/></p><p><br/></p><p>但在认真思考后，我认为VR的需求端逻辑牢固，供给端随着行业发展已接近成熟的临界点，关键设备VR头显的价格也已经十分性感，具备大规模销售的条件，同时，几个关键节点也有望点燃市场热情，打开万亿市场空间。</p><p><br/></p><p><br/></p><p><strong>从具体投资来看，VR已经到了爆发前夜，头显与游戏将率先掀动浪潮，VR其他应用内容和AR眼镜在一段时间磨合后会逐步兴起，引领我们进入长达五十年的“元宇宙”投资盛筵。</strong></p><p><br/></p><p><br/></p><p><strong>这一行业发展过程，可简单看作从偶尔消遣的“VR游戏机”，到高依赖度的“智能手机”，最后元宇宙接棒“互联网”，扩展人类生活。</strong></p><p><br/></p><p><br/></p><p>做出这样的判断，是基于行业实际进展与逻辑推理所得，本文将以五个理由阐述。仅供交流参考，不作投资建议。</p><p><br/></p><p><br/></p><p><strong>目录：</strong></p><p><br/></p><p><br/></p><p>（一）底层逻辑：人类自然的欲望需求</p><p><br/></p><p><br/></p><p>（二）行业发展：八十年风雨，始见终章</p><p><br/></p><p><br/></p><p>（三）市场空间：每年两万三千亿？</p><p><br/></p><p><br/></p><p>（四）关键事件：解锁市场热情的钥匙</p><p><br/></p><p><br/></p><p>（五）投资维度：产业宽泛，可投资公司众多</p><p><br/></p><p><br/></p><h2>01、底层逻辑：人类自然的欲望需求</h2><p><br/></p><p><br/></p><p>三年前，斯皮尔伯格执导的《头号玩家》横空出世，备受欢迎，尤其受到Z世代追捧。电影中的虚拟宇宙“绿洲”让人心弛神往，而它实际上就是对近期火热的Metaverse元宇宙概念最贴合的演绎，即创造独立于现实世界的虚拟数字第二世界，使用户能以数字身份自由生活。</p><p><br/></p><p>为什么人们会向往“绿洲”呢？</p><p><br/></p><p><br/></p><p>这实际上与人们为什么喜欢玩游戏、看电影是一个道理，一局游戏就是一次虚拟冒险，一场电影就是一次对自身现实之外生活的参与。所以，这都源于人类最自然的欲望需求，即适时离开真实世界，追求或参与或见证其他的生活，获得更多的乐趣，满足精神的愉悦和心灵的慰藉。</p><p><br/></p><p><br/></p><p>那么，怎样的游戏能成为爆款游戏？怎样的电影让人沉醉？</p><p><br/></p><p><br/></p><p>答：<strong>强烈的沉浸感。</strong>好的游戏或电影一定可以把我们带入其中，沉浸在剧中虚拟的喜怒哀乐，不能自拔，以至于暂时忘记真实世界。</p><p><br/></p><p><br/></p><p>我们在打游戏或看电影时，视角范围在60度到120度，当我们抬头或扭头时，便回归现实，仅是部分沉浸。而“绿洲”这样的虚拟元宇宙，每个角度的视角都是360度，属于完全沉浸，感受和真实世界别无二致，这种状态一定是更吸引人的，所以元宇宙的底层需求逻辑牢不可破。</p>', 1, 1, 1584282332, 1625367794, 1);
INSERT INTO `z_post` VALUES (20, '美军不打招呼撤离阿富汗最大军事基地，几小时后抢劫者闯入袭击……', '啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦hello', '<p><img src=\"/uploads/20211229/48ad6ea0ab6ee6739673a0fe77928b333632ea73.jpg\" style=\"max-width:100%;\"/><br/></p><p>在美军撤离驻阿富汗最大的军事基地之后仅几个小时，数十名抢劫者就闯入该基地。</p><p><img src=\"https://images.shobserver.com/export_img/2021/7/4/02de53e4-154b-409c-99cb-755101b00386.jpg\"/></p><p>这是7月2日在阿富汗帕尔万省拍摄的美国和北约军队撤离后的巴格拉姆空军基地。新华社 图</p><p>据美联社2日报道，巴格拉姆镇的地区行政长官拉菲（darwaish raufi）表示，由于美军的离开没有与当地官员进行任何协调，基地的大门没有任何安全保障，使得抢劫者可以在阿富汗军队接管基地之前非法进入。</p><p>拉菲说：“这些抢劫者被稍后赶到的阿富汗军队拦住了，一些人被捕，其余的人已被赶出基地。”这些抢劫者在被捕前袭击了多个建筑物。</p><p>拉菲表示，目前阿富汗国防军已完全控制了基地。</p><p>阿富汗国防部副发言人法瓦德·阿曼（fawad aman）当地时间7月2日在社交媒体上发布消息称，所有外国联军和美军已于昨晚离开巴格拉姆空军基地。基地已被移交给阿富汗国防和安全部队。</p><p>海外网7月3日消息，综合《华尔街日报》、俄罗斯卫星通讯社报道，在美军撤离阿富汗之际，美国官员透露，包含美驻阿富汗使馆人员在内的数千人或离开。</p><p>报道称，出于对安全的担忧，美方加强了驻阿富汗喀布尔大使馆的撤离计划。不仅包括美国驻阿富汗的上百名使馆人员，还有留在该国的数千名美国公民都可能被撤离。然而，虽然撤离的准备工作被加快，但美方并没有当下就实施的计划。</p><p>栏目主编：赵翰露</p><p>本文作者：澎湃新闻 南博一</p><p>文字编辑：杨蓉</p><p>题图来源：新华社</p><p>图片编辑：苏唯</p>', 0, 1, 1584282332, 1625367680, 2);
INSERT INTO `z_post` VALUES (21, 'hello呀', 'hello呀hello呀hello呀', '<p><img src=\"/uploads/20211229/a60a75bedada514c6a21a108b23dafc350a57ff0.jpg\" style=\"max-width:50%;\"/><br/></p>', 2, 1, 1640746991, 0, 1);

-- ----------------------------
-- Table structure for z_post_tag
-- ----------------------------
DROP TABLE IF EXISTS `z_post_tag`;
CREATE TABLE `z_post_tag`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL COMMENT '文章',
  `tag_id` int(11) NOT NULL COMMENT '标签',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `post_id`(`post_id`) USING BTREE,
  INDEX `tag_id`(`tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章标签关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_post_tag
-- ----------------------------
INSERT INTO `z_post_tag` VALUES (1, 1, 1);
INSERT INTO `z_post_tag` VALUES (7, 20, 5);
INSERT INTO `z_post_tag` VALUES (8, 20, 6);
INSERT INTO `z_post_tag` VALUES (9, 18, 7);
INSERT INTO `z_post_tag` VALUES (10, 7, 8);
INSERT INTO `z_post_tag` VALUES (11, 20, 1);

-- ----------------------------
-- Table structure for z_role
-- ----------------------------
DROP TABLE IF EXISTS `z_role`;
CREATE TABLE `z_role`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `permissions` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限规则ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_role
-- ----------------------------
INSERT INTO `z_role` VALUES (1, '测试', 1, '[\"*.#setting\",\"*.#admin-setting\",\"*.backend\\/role\\/index\",\"*.backend\\/role\\/create\",\"*.backend\\/role\\/update\",\"post.backend\\/role\\/change\",\"delete.backend\\/role\",\"*.backend\\/role\\/assign\",\"get.backend\\/admin\\/index\",\"*.backend\\/admin\\/create\",\"*.backend\\/admin\\/update\",\"post.backend\\/admin\\/change\",\"delete.backend\\/admin\",\"delete.backend\\/attachment\\/delete\",\"*.curd\",\"*.#centor\",\"*.curd\\/page\\/user\",\"*.curd\\/page\\/create\\/user\",\"*.curd\\/page\\/update\\/user\",\"post.curd\\/page\\/change\\/user\",\"delete.curd\\/page\\/delete\\/user\"]');

-- ----------------------------
-- Table structure for z_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `z_role_permission`;
CREATE TABLE `z_role_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` mediumint(8) UNSIGNED NOT NULL,
  `role_id` mediumint(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uid_group_id`(`admin_id`, `role_id`) USING BTREE,
  INDEX `uid`(`admin_id`) USING BTREE,
  INDEX `group_id`(`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组规则表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_role_permission
-- ----------------------------
INSERT INTO `z_role_permission` VALUES (10, 2, 1);
INSERT INTO `z_role_permission` VALUES (11, 3, 1);

-- ----------------------------
-- Table structure for z_tags
-- ----------------------------
DROP TABLE IF EXISTS `z_tags`;
CREATE TABLE `z_tags`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章标签' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_tags
-- ----------------------------
INSERT INTO `z_tags` VALUES (1, 'PHP');
INSERT INTO `z_tags` VALUES (3, '啦啦');
INSERT INTO `z_tags` VALUES (4, 'okok');
INSERT INTO `z_tags` VALUES (5, '美军');
INSERT INTO `z_tags` VALUES (6, '阿富汗');
INSERT INTO `z_tags` VALUES (7, 'vr');
INSERT INTO `z_tags` VALUES (8, '视频');

-- ----------------------------
-- Table structure for z_user
-- ----------------------------
DROP TABLE IF EXISTS `z_user`;
CREATE TABLE `z_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `age` tinyint(3) UNSIGNED NOT NULL DEFAULT 18 COMMENT '年龄',
  `sex` enum('1','2') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '性别',
  `status` enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '加入时间',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_user
-- ----------------------------
INSERT INTO `z_user` VALUES (1, '啦啦啦', '/uploads/20211229/a60a75bedada514c6a21a108b23dafc350a57ff0.jpg', 18, '1', '1', 1630166401, '$2y$10$ge8YNIeJsic0vxTAh/ba/eDtpRVP.UH7WmIFxs2Ox0Uu.PwzCnV0C');
INSERT INTO `z_user` VALUES (2, 'zsw', '/uploads/20211229/dc6eb27c92a378a7f03cd2e9c02f8845a56dc9b2.jpg', 18, '2', '1', 1630166401, '$2y$10$ge8YNIeJsic0vxTAh/ba/eDtpRVP.UH7WmIFxs2Ox0Uu.PwzCnV0C');

SET FOREIGN_KEY_CHECKS = 1;
