# Surface

## 描述

> 根据 Json 快速构建表格表单

## 使用

> 下载源码直接访问form.html和table.html查看试例

## 文档
[https://doc.zsw.ink](https://doc.zsw.ink)

## 示例 

**Form** 
[form.html](form.html)

**Table** 
[table.html](table.html)

## CDN

```
// Form
<script src="https://cdn.jsdelivr.net/gh/iszsw/surface-src/surface-form.js">
<script src="https://cdn.jsdelivr.net/gh/iszsw/surface-src/wangEditor.min.js"></script>

// Table
<script src="https://cdn.jsdelivr.net/gh/iszsw/surface-src/surface-table.js">
```

> Form 组件

![6jcp11.png](https://z3.ax1x.com/2021/03/26/6jcp11.png)

> Table组件

![6jcp11.png](https://z3.ax1x.com/2021/03/26/6jcSpR.png)
![6jcp11.png](https://z3.ax1x.com/2021/03/26/6jc96x.png)

## 作者

**zsw iszsw@qq.com**

主页 [zsw.ink](zsw.ink)

