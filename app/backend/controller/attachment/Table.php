<?php

namespace app\backend\controller\attachment;

use app\backend\model\Attachment as AttachmentModel;
use app\backend\logic\Uploader;
use app\backend\logic\Attachment as AttachmentLogic;
use surface\Component;
use surface\helper\FormAbstract;
use surface\helper\TableAbstract;
use surface\table\components\Button;
use surface\table\components\Column;
use surface\table\components\Expand;
use surface\table\components\Selection;

class Table extends TableAbstract
{

    private $rulePrefix = '/backend/attachment';

    public function search(): ?FormAbstract
    {
        return new Search();
    }

    public function header(): ?Component
    {
        return (new Component(['el' => 'div']))->children(
            [
                (new Component())->el('p')->children(
                    [
                        (new Component())->el('span')->class('title')->children(['附件管理']),
                        (new Component())->el('span')->class('describe')->children(['后台附件管理']),
                    ]
                ),
                (new Button('el-icon-upload2', '上传'))->createPage($this->rulePrefix.'/manage'),
                (new Button('el-icon-close', '删除'))->createSubmit(
                    ['method' => 'DELETE', 'data' => ['id'], 'url' => $this->rulePrefix.'/delete'],
                    '确认删除选中列',
                    'id'
                )->props('doneRefresh', true),
                (new Button('el-icon-refresh', '刷新'))->createRefresh(),
                (new Button('el-icon-search', '搜索'))->createSearch(),
            ]
        );
    }

    public function pagination(): ?Component
    {
        return (new Component())->props(
            [
                'async' => [
                    'url' => '',
                ],
            ]
        );
    }

    public function columns(): array
    {
        return [
            (new Selection('id', AttachmentModel::$labels['id'])),
            (new Expand('url', AttachmentModel::$labels['url']))->scopedSlots([new component(['el' => 'span', 'inject' => ['children']])]),
            (new Column('id', AttachmentModel::$labels['id']))->props('width', '50px'),
            (new Column('uploader_name', AttachmentModel::$labels['uploader']))->props(['width' => '100px', 'show-overflow-tooltip' => true]),
            (new Column('url', AttachmentModel::$labels['thumbnail']))->scopedSlots(
                [
                    (new Component())->el('el-image')->inject('attrs', ['src', 'array.preview-src-list'])->style('width', '80px'),
                ]
            )->props('width', '100px'),
            (new Column('name', AttachmentModel::$labels['name']))->props('show-overflow-tooltip', true),
            (new Column('ip', AttachmentModel::$labels['ip']))->props('width', '120px'),
            (new Column('size_label', AttachmentModel::$labels['size']))->props('width', '100px'),
            (new Column('wh', AttachmentModel::$labels['wh']))->props('width', '110px'),
            (new Column('mime', AttachmentModel::$labels['mime']))->props('width', '110px'),
            (new Column('suffix', AttachmentModel::$labels['suffix']))->props('width', '65px')->scopedSlots(
                [
                    (new Component(['el' => 'el-tag']))->props(['type' => 'success'])->inject('domProps', 'innerHTML')
                ]
            ),
            (new Column('create_time', AttachmentModel::$labels['create_time'])),

            (new Column('options', '操作'))->props('fixed', 'right')->props('width', '80px')
                ->scopedSlots(
                    [
                        (new Button('el-icon-delete', '删除'))
                            ->visible('_delete_visible')
                            ->createConfirm('确认删除数据？', ['method' => 'DELETE', 'data' => ['id'], 'url' => $this->rulePrefix.'/delete']),
                    ]
                ),
        ];
    }

    public function data($where = [], $order = '', $page = 1, $limit = 15): array
    {
        foreach ($where as $k => $v) {
            if (count($v) !== 3) {
                unset($where[$k]);
                continue;
            }
            switch ($v[0]) {
                case 'size':
                    $v[2] = [$v[2][0] * 1000, $v[2][1] * 1000];
                    $where[$k] = $v;
                    break;
                case 'suffix':
                    $extends = (new Uploader())->getExt();
                    $allow = [];
                    foreach ($v[2] as $e) {
                        $allow = array_merge($allow, $extends[$e] ?? []);
                    }
                    $v[2] = $allow;
                    $where[$k] = $v;
                    break;
            }
        }

        return AttachmentLogic::tableList($where, $order, $page, $limit);
    }


}
