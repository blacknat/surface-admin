<?php

namespace app\backend\controller;


use think\facade\Db;

class User extends BackendController
{

    /**
     *
     * 修改用户密码
     *
     * 返回null表示不修改
     *
     * @param $pwd   当前字段值
     * @param $post  当前提交的参数
     *
     * @return mixed|null
     */
    public static function password($pwd, $post)
    {
        return $pwd ? hashPassword($pwd) : null;
    }

    public function change($id)
    {
        $user = Db::name('user')->find($id);
        if ($user) {
            Db::name('user')->where(['id'=>$id])->update(['status' => $user['status'] == 1 ? 0 : 1]);
        }
        return _success('修改成功');
    }

}
