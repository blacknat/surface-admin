<?php

namespace app\backend\controller;

use app\api\annotation\illustrate\Route;
use app\backend\services\auth\Authority;
use app\backend\services\auth\Permission;
use think\facade\Cache;
use think\facade\Log;
use think\facade\View;

class Index extends BackendController
{

    /**
     * 获取菜单
     */
    private function assignMenu()
    {
        $sessionKey = 'backend:admin_menu';
        if ((bool)env('APP_DEBUG', false) || ! ($menus = session($sessionKey)))
        {
            $permissions = $this->app->auth->getPermissions();
            $menus = $this->getMenus($permissions);
            session($sessionKey, $menus);
        }
        return $menus ?: [];
    }

    private function getMenus($permissions)
    {
        $menus = [];
        /** @var Authority $auth */
        $auth = $this->app->auth;
        /** @var Permission $permission */
        foreach ($permissions as $k => $permission)
        {
            if ($auth->can(['route' => $permission->route, 'menu' => true]))
            {
                $menus[$k] = array_merge(['img' => '', 'icon' => ''], $permission->getData());
                $menus[$k]['children'] = $this->getMenus($permission->getChildren());
            }
        }

        return $menus;
    }

    public function index()
    {
        if (!session('?admin')) {
            return redirect('/backend/login');
        }

        return View::fetch(
            '', [
                  'menu'    => $this->assignMenu(),
              ]
        );
    }

    public function clean()
    {
        Cache::clear();
        Log::clear();
        del_dir(runtime_path('temp'));
        return _success("缓存文件清理完成");
    }


    public function home()
    {
        return View::fetch('', [
            'sysInfo' => $this->get_sys_info()
        ]);
    }

    public function logout()
    {
        return _success('退出成功', [], (string)url('login/index'));
    }

    protected function get_sys_info()
    {
        $sys_info['os'] = PHP_OS;
        $sys_info['zlib'] = function_exists('gzclose') ? 'YES' : 'NO';//zlib
        $sys_info['safe_mode'] = (boolean)ini_get('safe_mode') ? 'YES' : 'NO';//safe_mode = Off
        $sys_info['timezone'] = function_exists("date_default_timezone_get") ? date_default_timezone_get() : "no_timezone";
        $sys_info['curl'] = function_exists('curl_init') ? 'YES' : 'NO';
        $sys_info['web_server'] = $this->request->server('SERVER_SOFTWARE');
        $sys_info['php_version'] = phpversion();
        $sys_info['ip'] = $this->request->ip();
        $sys_info['file_upload'] = @ini_get('file_uploads') ? ini_get('upload_max_filesize') : 'unknown';
        $sys_info['max_ex_time'] = @ini_get("max_execution_time").'s'; //脚本最大执行时间
        $sys_info['domain'] = $this->request->server('HTTP_HOST');
        $sys_info['memory_limit'] = ini_get('memory_limit');
        $sys_info['version'] = 'V 1.0.0';
        $mysqlInfo = \think\facade\Db::query("SELECT VERSION() as version");
        $sys_info['mysql_version'] = $mysqlInfo[0]['version'];
        if (function_exists("gd_info"))
        {
            $gd = gd_info();
            $sys_info['gd_info'] = $gd['GD Version'];
        } else
        {
            $sys_info['gd_info'] = "未知";
        }

        return $sys_info;
    }

    public function info(){
        return view();
    }

}
