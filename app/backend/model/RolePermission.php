<?php
namespace app\backend\model;

use app\backend\model\enum\Status;
use think\model\Pivot;

class RolePermission extends Pivot
{

    public static $labels = [
        'id'             => 'ID',
        'title'          => '角色名',
        'status'         => '状态',
        'rules'          => '权限'
    ];

    public static function onBeforeDelete($model)
    {
        AuthGroupAccess::destroy(function ($query) use ($model) {
            $query->where(['group_id'=>$model->id]);
        });
    }

    public static $statusLabels = [
        Status::STATUS_NORMAL => '正常',
        Status::STATUS_DISABLE => '禁止',
    ];

    protected function setRulesAttr($val)
    {
        return is_array($val) ? implode(',', array_filter($val)) : $val;
    }

}
