<?php
namespace app\backend\services\auth;

/**
 * @package auth
 * Author: zsw zswemail@qq.com
 */
interface RoleInterface
{

    /**
     * 获取当前角色权限
     *
     * @return mixed
     */
    public function permissions();


}
