<?php
namespace app\backend\services\auth;

/**
 * @package auth
 * Author: zsw zswemail@qq.com
 */
interface UserInterface
{

    /**
     * 获取所有角色
     *
     * @return array<RoleInterface>
     */
    public function roles();

}
